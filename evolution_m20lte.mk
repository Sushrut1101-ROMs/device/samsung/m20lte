$(call inherit-product, device/samsung/m20lte/full_m20lte.mk)

# Inherit some common Evolution X stuff!
$(call inherit-product, vendor/evolution/config/common_full_phone.mk)

PRODUCT_NAME := evolution_m20lte
